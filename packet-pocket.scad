// Based on the spicerack line.
//
// This is just a little standalone pocket to hold things like taco seasoning
// packets.  Should be easy peasy, at least in theory...

$rack_depth   = 115;
$width = 25.4 * 1.5 + 3;
$wall = 2;
$height = 25.4 * 2.6;
$lip = 25.4/2;

    
module rack() {
    $wire_diameter = 2.15;
    $wire_between  = 16.1 - $wire_diameter;

    rotate([270,0,0])
        for(count = [0:1:19])
            translate([$wire_between*count, 0, 0]) linear_extrude(150) circle($wire_diameter + .25);
        
}

module base_unit() {
    linear_extrude($height)
        square([$width, $rack_depth]);
}

module spicerack() {
    difference() {
        base_unit();
        
        translate([$wall,$wall,6]) linear_extrude($height) square([$width - $wall*2, $rack_depth - $wall*2]);
        translate([-$wall,$wall + $lip, $lip + 6]) linear_extrude($height) square([$width + $wall*2, $rack_depth - $wall*2 - $lip*2]);
        translate([7, -1, 0]) rack();
    }
}

spicerack();
