$jar_diameter = 45;
$rack_depth   = 115;
$jar_spacing  = 5;
$jar_columns  = 4;
$fn = 100;
    
module rack() {
    $wire_diameter = 2.15;
    $wire_between  = 16.1 - $wire_diameter;

    rotate([270,0,0])
        for(count = [0:1:19])
            translate([$wire_between*count, 0, 0]) linear_extrude(150) circle($wire_diameter + .25);
        
}

module base_unit() {
    $width = ($jar_diameter * $jar_columns) + ($jar_spacing * ($jar_columns + 2));

    linear_extrude(40)
        square([$width, $rack_depth]);
}

module spicejar(jd) {
    linear_extrude(112) circle(d = jd+1);
}

module spiceblock() {
    difference() {
        base_unit();
        translate([7, -1, 0]) rack();
    }
}

module spicerack() {
    difference() {
        spiceblock();
        
        translate([-1,-1,20]) linear_extrude(21) square([300, $rack_depth/2+1]);

        for (row = [0:1:1])
            for (col = [0:1:$jar_columns-1])
                translate([$jar_diameter/2 + $jar_spacing*1.5 + (col * ($jar_diameter + $jar_spacing)), $jar_diameter/2 + $jar_spacing + ($rack_depth/2*row), 5 + (15 * row)]) spicejar($jar_diameter);
    }
}

module spiceorg() {
    $width = ($jar_diameter * $jar_columns) + ($jar_spacing * ($jar_columns + 2));
    
    $bigjar = 70;
    $medjar = 58;

    difference() {
      spiceblock();

      // Step Down
      translate([-1,-1,20]) linear_extrude(21) square([300, $rack_depth/2+3]);

      // Front Sections
      secwidth = ($width - 8) / 3;
      for (sec = [1:1:3])
          translate([2*sec+(secwidth*(sec-1)), 2, 5] )
            linear_extrude(42)
              square([secwidth, $rack_depth/2]);
 
      // Jar Holders
      translate([$jar_diameter - 7, $rack_depth/2 + ($rack_depth/2 - $jar_diameter + 5), 20]) spicejar($bigjar);
      translate([$jar_diameter + $jar_diameter * 2 - 24, $rack_depth/2 + ($rack_depth/2 - $jar_diameter + 5), 20]) spicejar($bigjar);
      translate([$jar_diameter + $jar_diameter * 3 - 2, $rack_depth/2 + ($rack_depth/2 - $jar_diameter + 12), 20]) spicejar($medjar);
    }
}

spiceorg();
