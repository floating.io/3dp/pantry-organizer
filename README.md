# pantry-organizer

OpenSCAD source files for my personal pantry organizer.

Because, why not?

Of course, these are just thrown together.  If I keep printing new
pantry organizers (I already have one additional idea!), then I might
go through and fix this up to make it a proper project.  Right now
it's just for documentation -- and also so you can point and laugh.

Attempt to use these at your own risk. =)

Oh, license: non-commercial use and remixing is fine, so long as I am
referenced as the original creator and derivatives remain licensed
with a non-commercial requirement.  Want to sell my organizers?  Drop
me a line, either here on gitlab.com, or over on
[floating.io](https://floating.io).  There's a feedback link over
there, though I might be slowish to respond.

Not that I think anyone will be interested enough, but oh well. =)

## Print Settings

I printed these on an X1 Carbon using the following settings:

- Standard .20mm layer height
- Lightning Infill (15%)

And... that's about it.  Everything else is default.  I've been
using Bambu's PLA Matte since that's what I have on hand.
