$jar_diameter = 45;
$rack_depth   = 115;
$jar_spacing  = 5;
$jar_columns  = 4;

    
module rack() {
    $wire_diameter = 2.15;
    $wire_between  = 16.1 - $wire_diameter;

    rotate([270,0,0])
        for(count = [0:1:19])
            translate([$wire_between*count, 0, 0]) linear_extrude(150) circle($wire_diameter + .25);
        
}

module base_unit() {
    $width = ($jar_diameter * $jar_columns) + ($jar_spacing * ($jar_columns + 2));

    linear_extrude(40)
        square([$width, $rack_depth]);
}

module spicejar() {
    linear_extrude(112) circle(d = $jar_diameter+1);
}

module spicerack() {
    difference() {
        base_unit();
        translate([7, -1, 0]) rack();
        translate([-1,-1,20]) linear_extrude(21) square([300, $rack_depth/2+1]);
        
        for (row = [0:1:1])
            for (col = [0:1:$jar_columns-1])
                translate([$jar_diameter/2 + $jar_spacing*1.5 + (col * ($jar_diameter + $jar_spacing)), $jar_diameter/2 + $jar_spacing + ($rack_depth/2*row), 5 + (15 * row)]) spicejar();
    }
}

spicerack();
